const express = require('express');
const app = express();
app.use(express.static(__dirname+'TEST_NODE_APPLICATION/')); 
const bodyParser = require('body-parser');
const port = 3000;

//Json parer
app.use(
    bodyParser.urlencoded({
        extended : true
    })
);

app.use(bodyParser.json());

app.use((req, res, next)=>{
    if(req.headers.token == 'NODEJSAPP'){
        req.skip = true;
    } else {
        req.skip = false;
    }
    next();
})
app.route('/').get((req,res)=>{
    console.log(new Date()+'accessed home page')
    res.status(200).send('Hello Please select your Direction')
})
app.route('/up').get((req,res)=>{
    console.log(new Date()+'accessed home page up')
    res.status(200).send(JSON.stringify({"timestamp":new Date(),"direction" : "up"}));
})
app.route('/down').get((req,res)=>{
    console.log(timeStamp+'accessed home page down')
    res.status(200).send(JSON.stringify({"timestamp":new Date(),"direction" : "down"}));
})
app.route('/left').get((req,res)=>{
    console.log(timeStamp+'accessed home page left')
    res.status(200).send(JSON.stringify({"timestamp":new Date(),"direction" : "left"}));
})
app.route('/right').get((req,res)=>{
    console.log(timeStamp+'accessed home page right')
    res.status(200).send(JSON.stringify({"timestamp":new Date(),"direction" : "right"}));
})

app.route('/auth').post((req,res) => {
    console.log('accessed auth page')
    if(req.skip)
    res.send('OK, Authenticated');
    else
    res.send('NO Access');
});

app.listen(port,()=>{
    console.log("server started on port", + port);
});

// To handle incorrect url
app.get("*", function(req, res){
    res.status(503).sendFile(__dirname+'/resources/404.jpg');
})